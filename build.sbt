val tapirVersion = "1.2.10"

// use this for proto without any grpc-fs2
//Compile / PB.targets := Seq(
//  scalapb.gen() -> (Compile / sourceManaged).value / "scalapb"
//)

lazy val rootProject = (project in file("."))
  .enablePlugins(Fs2Grpc)
  .settings(
    Seq(
      name := "akka-alternatives",
      version := "0.1.0-SNAPSHOT",
      organization := "com.softwaremill",
      scalaVersion := "2.13.10",
      libraryDependencies ++= Seq(
        "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-bundle" % "1.2.10",
        "com.softwaremill.sttp.tapir" %% "tapir-prometheus-metrics" % tapirVersion,
        "com.softwaremill.sttp.tapir" %% "tapir-json-spray" % "1.2.10",
        "com.softwaremill.sttp.client3" %% "spray-json" % "3.8.3",
        "pl.iterators" %% "kebs-spray-json" % "1.9.4",
        "com.softwaremill.sttp.tapir" %% "tapir-http4s-server" % tapirVersion,
        "org.http4s" %% "http4s-ember-server" % "0.23.18",
        "ch.qos.logback" % "logback-classic" % "1.4.5",
        "com.softwaremill.sttp.tapir" %% "tapir-sttp-stub-server" % tapirVersion % Test,
        "org.scalatest" %% "scalatest" % "3.2.15" % Test,
        // grpc
        //        "com.thesamet.scalapb" %% "scalapb-runtime" % scalapb.compiler.Version.scalapbVersion % "protobuf",
        //        "com.thesamet.scalapb" %% "scalapb-runtime-grpc" % scalapb.compiler.Version.scalapbVersion,
        //        "org.typelevel" %% "cats-core" % "2.9.0",
        //        "org.typelevel" %% "cats-effect" % "3.4.8",
        //        "io.grpc" % "grpc-netty" % scalapb.compiler.Version.grpcJavaVersion,
        //        "io.grpc" % "grpc-services" % scalapb.compiler.Version.grpcJavaVersion,
        // fs2-grpc
        "com.thesamet.scalapb" %% "scalapb-runtime" % scalapb.compiler.Version.scalapbVersion % "protobuf",
        "org.typelevel" %% "cats-core" % "2.9.0",
        "org.typelevel" %% "cats-effect" % "3.4.8",
        "io.grpc" % "grpc-netty" % scalapb.compiler.Version.grpcJavaVersion,
        "io.grpc" % "grpc-services" % scalapb.compiler.Version.grpcJavaVersion
      )
    )
  )