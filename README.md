### Alternatives to akka-http and akka-grpc

#### Akka http
- [http4s](https://http4s.org/v0.23/docs/quickstart.html)
  - IO based
  - similar syntax to akka
  - no support for Spray JSON
- [x] [tapir](https://tapir.softwaremill.com/en/latest/)
  - IO/Future/ZIO based
  - tried with IO (http4s backend)
  - support for Spray JSON
  - migration guide https://softwaremill.com/migrating-from-akka-http-to-tapir/
- [ZIO](https://zio.dev/)
  - not investigated

#### Akka gRPC
- [plain ScalaPB gRPC](https://scalapb.github.io/docs/grpc/#creating-a-service)
  - no framework, simple gRPC based on java stubs
  - can be wrapped in `Futures`
- [x] [fs2-gRPC](https://github.com/typelevel/fs2-grpc)
  - IO based
- [tapir gRPC](https://tapir.softwaremill.com/en/latest/grpc.html)
  - experimental

#### Done
- [x] main uses scala2 + scala3 branch
- [x] working http routing (scala2 + scala3)
- [x] working grpc server + client based on java + futures (scala2)
- [x] working grpc server + client based on f2s
- [ ] spray auto (scala3) 
  - not working probably because of `tapir-json-spray`
  - ```
    build.sbt
      "pl.iterators" %% "kebs-spray-json" % "1.9.3" cross CrossVersion.for3Use2_13,
      "io.github.paoloboni" %% "spray-json-derived-codecs" % "2.3.5",
    ```
  - ```
    [error] Modules were resolved with conflicting cross-version suffixes in ProjectRef(uri("file:/akka-alternatives/"), "rootProject"):
    [error]    io.spray:spray-json _3, _2.13
    ```
- [x] kebs (scala2)
- [x] [swagger](https://tapir.softwaremill.com/en/latest/docs/openapi.html)
- [x] [metrics](https://tapir.softwaremill.com/en/latest/server/observability.html#prometheus-metrics)
- [x] error handling
  - IO
  - Future
  - thrown Exception
  - Either
  - custom status codes
- [x] [testing routes](https://tapir.softwaremill.com/en/v0.17.4/testing.html)

#### In Progress
- migrate a project with both http routing and grpc (EventService)
- ? [reactive mongo IO](https://kirill5k.github.io/mongo4cats/docs/)
