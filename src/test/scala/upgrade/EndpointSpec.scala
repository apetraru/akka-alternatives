package upgrade

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import org.scalatest.EitherValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sttp.client3.sprayJson._
import sttp.client3.testing.SttpBackendStub
import sttp.client3.{HttpError, UriContext, basicRequest}
import sttp.model.StatusCode
import sttp.tapir.integ.cats.CatsMonadError
import sttp.tapir.server.stub.TapirStubInterpreter
import upgrade.tapir.Endpoints
import upgrade.tapir.JsonSupport._
import upgrade.tapir.model.{Author, Book}


class EndpointSpec extends AnyFlatSpec with Matchers with EitherValues {
  val expectedBook: Book = Book("The Sorrows of Young Werther", 1774, Author("Johann Wolfgang von Goethe"))

  // TODO can we use a RecordingSttpBackend?
  it should "get a book" in {
    // given
    val backendStub = TapirStubInterpreter(SttpBackendStub(new CatsMonadError[IO]()))
      .whenServerEndpoint(Endpoints.futureEndpoint)
      .thenRunLogic()
      .backend()


    // when
    val error = basicRequest
      .get(uri"http://test.com/book-future")
      .response(asJson[Book])
      .send(backendStub)

    // then
    // 1st request throws error
    // there's probably a better way to do this
    error.map(_.body.left.value shouldBe HttpError(body = "\"Thrown custom exception\"", statusCode = StatusCode.BadRequest)).unwrap

    // and then
    val success = basicRequest
      .get(uri"http://test.com/book-future")
      .response(asJson[Book])
      .send(backendStub)
    // 2nd request succeeds
    success.map(_.body.value shouldBe expectedBook).unwrap

  }

  implicit class Unwrapper[T](t: IO[T]) {
    def unwrap: T = t.unsafeRunSync()
  }
}