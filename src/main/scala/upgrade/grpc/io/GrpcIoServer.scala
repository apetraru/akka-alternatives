package upgrade.grpc.io

import cats.effect.{IO, Resource, ResourceApp}
import example.proto.example.{GreeterFs2Grpc, HelloReply, HelloRequest}
import fs2.grpc.syntax.all._
import io.grpc.Metadata
import io.grpc.netty.NettyServerBuilder
import io.grpc.protobuf.services.ProtoReflectionService


class GrpcIoServer extends GreeterFs2Grpc[IO, Metadata] {
  override def sayHello(request: HelloRequest, ctx: Metadata): IO[HelloReply] =
    IO.pure(HelloReply(s"Hello, ${request.name}")) <* IO.println(s"gRPC IO processed $request")
}

object GrpcIoServer extends ResourceApp.Forever {
  override def run(args: List[String]) =
    for {
      service <- GreeterFs2Grpc.bindServiceResource(new GrpcIoServer)
      server <- NettyServerBuilder.forPort(5002).addService(service)
        .addService(ProtoReflectionService.newInstance())
        .resource[IO]
      _ <- Resource.eval(IO.println("Server started"))
    } yield server.start()

}
