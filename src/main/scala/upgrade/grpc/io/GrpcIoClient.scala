package upgrade.grpc.io

import cats.effect.{IO, IOApp}
import example.proto.example.{GreeterFs2Grpc, HelloRequest}
import io.grpc.Metadata
import io.grpc.netty.NettyChannelBuilder
import fs2.grpc.syntax.all._
import cats.implicits._

import scala.concurrent.duration.DurationInt

class GrpcIoClient(client: GreeterFs2Grpc[IO, Metadata]) {
  def sayHello(name: String): IO[String] =
    client.sayHello(HelloRequest(name), new Metadata())
      .map(_.message)
}

object GrpcIoClient extends IOApp.Simple {
  def resource() =
    for {
      channel <- NettyChannelBuilder
        .forTarget("localhost:5002")
        .usePlaintext()
        .resource[IO]
      client <- GreeterFs2Grpc.stubResource[IO](channel)
    } yield new GrpcIoClient(client)

  override def run: IO[Unit] =
    GrpcIoClient.resource().use {client =>
      Seq.range(1, 5).map { i =>
        val name = s"Gigi$i"
        for {
          _ <- IO.println(s"Sending $name to gRPC IO server")
          _ <- IO.sleep(1.second)
          response <- client.sayHello(name)
          _ <- IO.println(s"Received $response")
        } yield ()
      }.sequence_
    }
}
