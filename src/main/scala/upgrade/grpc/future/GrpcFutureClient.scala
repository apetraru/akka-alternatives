package upgrade.grpc.future

import example.proto.example.GreeterGrpc.GreeterStub
import example.proto.example.{GreeterGrpc, HelloRequest}
import io.grpc.{ManagedChannel, ManagedChannelBuilder}

import java.util.concurrent.TimeUnit
import java.util.logging.Logger
import scala.concurrent.ExecutionContext.Implicits.global

object GrpcFutureClient {
  def apply(host: String, port: Int) = {
    val channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build
    val stub = GreeterGrpc.stub(channel)
    new GrpcFutureClient(channel, stub)
  }

  def main(args: Array[String]): Unit = {
    val client = GrpcFutureClient("localhost", 9001)
    try {
      client.greet("vasile")
    }
    finally {
      client.shutdown()
    }
  }
}

class GrpcFutureClient(val channel: ManagedChannel, val stub: GreeterStub) {
  val logger = Logger.getLogger(classOf[GrpcFutureClient].getName)

  def shutdown() = channel.shutdown().awaitTermination(5, TimeUnit.SECONDS)

  def greet(name: String) = {
    logger.info(s"Trying to greet $name.")
    val request = HelloRequest(name)
    try {
      val response = stub.sayHello(request)
      for {
        reply <- response
      } logger.info(s"Greeting: ${reply.message}")
      Thread.sleep(500)
    }
    catch {
      case e: Exception => {
        logger.severe(s"RPC failed: ${e.getMessage}")
      }
    }
  }
}


