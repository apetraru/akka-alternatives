package upgrade.tapir

import pl.iterators.kebs.json.KebsSpray
import spray.json._

object JsonSupport extends DefaultJsonProtocol with KebsSpray {
  //  implicit val authorFormat: RootJsonFormat[Author] = jsonFormat1(Author)
  //  implicit val bookFormat: RootJsonFormat[Book] = jsonFormat3(Book)
}
