package upgrade.tapir.model

case class CustomException(message: String) extends RuntimeException(message: String) {
}
