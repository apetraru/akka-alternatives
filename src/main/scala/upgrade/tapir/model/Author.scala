package upgrade.tapir.model

import sttp.tapir.Schema.annotations.description

@description("Author")
case class Author(name: String)
