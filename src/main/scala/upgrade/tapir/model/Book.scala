package upgrade.tapir.model

import sttp.tapir.Schema.annotations.description

@description("Description of a book")
case class Book(@description("title of the book") title: String, year: Int, author: Author)
